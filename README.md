﻿# bitcoinlike

# Mining processes
Bitcoin’s decentralized consensus emerges from the interplay of four processes that occur independently on nodes across the network:
1. Independent verification of each transaction, by every full node, based on a comprehensive list of criteria
11. Won't be covered in this project.
1. Independent aggregation of those transactions into new blocks by mining nodes, coupled with demonstrated computation through a Proof-of-Work algorithm
11. Only the proof-of-work algorithm to be implemented, but it will operate on empty blocks.
1. Independent verification of the new blocks by every node and assembly into a chain
11. All to be implemented.
1. Independent selection, by every node, of the chain with the most cumulative computation demonstrated through Proof-of-Work
11. All to be implemented.