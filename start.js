﻿// Transpile all code following this line with babel and use 'env' (aka ES6) preset.
console.log('Starting Application');

require("@babel/register")({
    presets: ["@babel/preset-env"]
});

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

Promise.all([Promise.resolve(async () => {
    await sleep(1000);
    console.log("First statement");
})]);

console.log("Second statement");


module.exports = require('./src/app.js');

console.log('Application Started Successfully');