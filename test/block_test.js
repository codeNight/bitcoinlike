﻿import Assert from 'assert';
import BigInt from 'big-integer';
import { describe } from 'mocha';

import Block from '../src/models/block';
import CryptoUtils from '../src/utils/crypto_utils';

describe('Block', function () {
    const PREV_HASH = '30c6ff7a44f7035af933babaea771bf177fc38f06482ad06434cbcc04de7ac14';
    const TIMESTAMP = Date.now();
    const DIFFICULTY_BITS = 5; // The generated hash should at least have a leading zero
    const INDEX = 3;
    const PAYLOAD = "To Prove that Ali Abdelhafez Has a heart";
    const PAYLOAD_SHA256_HASH = '9043325cc297d2dffd575795c467c056b96d88a6a9cb011c28db21e81f6b16fc';

    describe('constructor', () => {
        it('should create a block with the same provided parameter', () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);

            const blockHeader = block.blockHeader;

            Assert.equal(PREV_HASH, blockHeader.prev_hash, "prev_hash value is not the same as expected.");
            Assert.equal(TIMESTAMP, blockHeader.timestamp, "timestamp value was changed.");
            Assert.equal(DIFFICULTY_BITS, blockHeader.difficulty_bits, "difficulity_bits value was changed");
            Assert.equal(INDEX, blockHeader.index, "block height was changed");
            Assert.equal(PAYLOAD_SHA256_HASH, blockHeader.payloadHash, "Block payload was changed.");

            Assert.equal(PAYLOAD, block.payload);
        });
    });

    describe("mine()", () => {
        it("shouldn't fail.", () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);
            const blockHeader = block.blockHeader;

            // Try mining block
            block.mine();

            Assert.ok(true);
        });

        it("shouldn't change header constant parameters.", () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);
            const blockHeader = block.blockHeader;

            // Try mining block
            block.mine();

            Assert.equal(PREV_HASH, blockHeader.prev_hash, "prev_hash value is not the same as expected.");
            Assert.equal(TIMESTAMP, blockHeader.timestamp, "timestamp value was changed.");
            Assert.equal(DIFFICULTY_BITS, blockHeader.difficulty_bits, "difficulity_bits value was changed");
            Assert.equal(INDEX, blockHeader.index, "block height was changed");
            Assert.equal(PAYLOAD_SHA256_HASH, blockHeader.payloadHash, "Block payload was changed.");

            Assert.equal(PAYLOAD, block.payload);
        });

        it('should set some values as a result of mining process like timeElapsed, blockHash and nonce', () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);
            const blockHeader = block.blockHeader;

            // Try mining block
            block.mine();

            Assert.notStrictEqual(blockHeader.nonce, undefined);
            Assert.notStrictEqual(blockHeader.nonce, null);


            Assert.notStrictEqual(block.timeElapsed, undefined);
            Assert.notStrictEqual(block.timeElapsed, null);
        });

        it('should find a hash that is less that the target, and the taget is calculated using difficulity bits', () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);

            // Try mining block
            block.mine();

            const target = BigInt(1).shiftLeft(256 - DIFFICULTY_BITS);
            const blockHash = BigInt(block.getBlockHash(), 16);

            Assert.ok(blockHash.lesser(target));
        });
    });

    describe('getBlockHash', () => {
        it('should return the hash of the block header always', () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);

            // Try mining block
            block.mine();

            const blockHeader = block.blockHeader;
            const blockHeaderHash = CryptoUtils.getDoubleHash(JSON.stringify(blockHeader)); // to enforce the hash calculation algoritm

            Assert.equal(block.getBlockHash(), blockHeaderHash);
        });

        it('should return the same hash if called severial times after mining', () => {
            const block = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);

            // Try mining block
            block.mine();

            const hash1 = block.getBlockHash();
            const hash2 = block.getBlockHash();

            Assert.equal(hash1, hash2);
        });
    });
});
