﻿import Assert from 'assert';
import { describe } from 'mocha';

import CryptoUtils from '../src/utils/crypto_utils';

describe('CryptoUtils', function () {
    const TEST_STRING = "Test String";
    const TEST_STRING_REAL_HASH = "30c6ff7a44f7035af933babaea771bf177fc38f06482ad06434cbcc04de7ac14";

    describe('GetHash', () => {
        it('Should return the correct hash value', () => {
            const hash = CryptoUtils.getHash(TEST_STRING);
            Assert.equal(hash, TEST_STRING_REAL_HASH, "Hash function didn't generate that same SHA256 Hash result");
        });

        it('Should return the correct hash value, and return the same value if hashed again', () => {
            CryptoUtils.getHash(TEST_STRING);
            CryptoUtils.getHash(TEST_STRING);
            CryptoUtils.getHash(TEST_STRING);
            const hash = CryptoUtils.getHash(TEST_STRING);
            Assert.equal(hash, TEST_STRING_REAL_HASH, "Hash function didn't generate that same SHA256 Hash result after calling 'getHash' multiple times. It is either that the input changes or the hash function have some sort of an internal state.");
        });
    });

    describe('GetDoubleHash', () => {
        const TEST_STRING_REAL_DOUBLE_HASH = "25771106c400d82c146a678436723c35485f5c09617cbb72cda15e8376a1e732";
        it('Should return the correct hash of hash value', () => {
            const hash = CryptoUtils.getDoubleHash(TEST_STRING);
            Assert.equal(hash, TEST_STRING_REAL_DOUBLE_HASH, "Hash function didn't return the correct hash of hash value");
        });

        it('Should return the correct hash of hash value, and return the same answer after calling again', () => {
            CryptoUtils.getDoubleHash(TEST_STRING);
            CryptoUtils.getDoubleHash(TEST_STRING);
            CryptoUtils.getDoubleHash(TEST_STRING);
            const hash = CryptoUtils.getDoubleHash(TEST_STRING);
            Assert.equal(hash, TEST_STRING_REAL_DOUBLE_HASH, "Hash function didn't generate that same SHA256 Hash" +
                "result after calling 'getHash' multiple times.It is either that the input changes or the hash function have some sort of an internal state.");
        });
    });
});

