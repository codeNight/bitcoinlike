﻿import Assert from 'assert';
import Sinon from 'sinon';
import { describe, after, afterEach } from 'mocha';
import Levelup from 'levelup';
import Chai from 'chai';

import { expect as Expect } from 'chai';

import ChaiAsPromised from 'chai-as-promised';

Chai.use(ChaiAsPromised);

import BlockStorageService from '../src/services/block_storage_service';
import Block from '../src/models/block';

describe('BlockStorageService Unit Tests', function () {
    const PREV_HASH = '30c6ff7a44f7035af933babaea771bf177fc38f06482ad06434cbcc04de7ac14';
    const TIMESTAMP = Date.now();
    const DIFFICULTY_BITS = 5; // The generated hash should at least have a leading zero
    const INDEX = 3;
    const PAYLOAD = "To Prove that Ali Abdelhafez Has a heart";
    var blockStorageService = new BlockStorageService('blockTestStore');

    describe('BlockStorageService', function () {
        beforeEach(() => {
            this.Sandbox = Sinon.createSandbox();
        });

        afterEach(() => {
            this.Sandbox.restore();
        });

        var dummyBlock = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);

        it('should call underlying get with right parameters', async () => {

            var getStub = this.Sandbox
                .stub(Levelup.prototype, 'get')
                .returns(Promise.resolve(dummyBlock.toJson()));

            var data = await blockStorageService.get(dummyBlock.getBlockHash());

            Assert.equal(dummyBlock.toJson(), data);
            return Sinon.assert.calledWith(getStub, dummyBlock.getBlockHash())
        });
    
        it('should call underlying put with right parameters', async () => {

            var putStub = this.Sandbox
                .stub(Levelup.prototype, 'put')
                .returns(Promise.resolve());

            await blockStorageService.put(dummyBlock);

            return Sinon.assert.calledWith(putStub, dummyBlock.getBlockHash(), dummyBlock.toJson());
        });


        it('Should return a rejected promise when get method in levelup method throws an unexpected exception',  () => {
            const thrownException = 'Dummy Exception';

            this.Sandbox
                .stub(Levelup.prototype, 'get')
                .throws(new Error(thrownException));

            return Expect(blockStorageService.get('someHash')).to.be.rejectedWith(thrownException);
        });

        it('Should return a rejected promise when it failes to get a value', () => {
            const thrownException = 'Dummy Exception';

            this.Sandbox
                .stub(Levelup.prototype, 'get')
                .returns(Promise.reject(thrownException)); 

            return Expect(blockStorageService.get('someHash')).to.be.rejectedWith(thrownException);
        });

        it('Should return a rejected promise when "put" method in levelup method throws an unexpected exception', () => {
            const thrownException = 'Dummy Exception';

            this.Sandbox
                .stub(Levelup.prototype, 'put')
                .throws(new Error(thrownException));

            return Expect(blockStorageService.put(dummyBlock)).to.be.rejectedWith(thrownException);
        });

        it('Should return a rejected promise when it failes to "put" a value', () => {
            const thrownException = 'Dummy Exception';

            this.Sandbox
                .stub(Levelup.prototype, 'put')
                .returns(Promise.reject(thrownException));

            return Expect(blockStorageService.put(dummyBlock)).to.be.rejectedWith(thrownException);
        });

        it('Should write a value to a store with no problems', () => {
            return Expect(blockStorageService.put(dummyBlock)).not.to.be.rejected;
        });

        it('should read a value written to a store with no problems', () => {
            const blockString = dummyBlock.toJson();

            return Expect(blockStorageService.get(dummyBlock.getBlockHash())).to.eventually.equal(blockString);
        });
    });
});
