﻿// this is a soft miner, doesn't have database integration yet.
// Should run as a daemon thread in the background.
import Block from '../models/block';
import CryptoUtils from '../utils/crypto_utils';
import BlockStorageService from '../services/block_storage_service.js';

import FileStream from 'fs';

class MiningService {
    static async start() {
        const PREV_HASH = '0';
        const TIMESTAMP = Date.now();
        const DIFFICULTY_BITS =12;
        const INDEX = 1;
        const PAYLOAD = "To Prove that Ali Abdelhafez Has a heart";

        const genesisBlock = new Block(PREV_HASH, TIMESTAMP, DIFFICULTY_BITS, INDEX, PAYLOAD);
        genesisBlock.mine();

        FileStream.writeFile('./src/resources/genesis.json', genesisBlock.toJson(), (err) => {
            // throws an error, you could also catch it here
            if (err) throw err;

            // success case, the file was saved
            //console.log('Genesis written successfully'); 
        });

        //console.log(genesisBlock.toJson());
        //console.log('======================================================');
        var headBlock = genesisBlock;

        var blockStorageService = new BlockStorageService();

        var minedBlocks = [];
        for (var i = 0; i < 10; i++) {
            headBlock.mine();
            minedBlocks.push(headBlock);

            //console.log(headBlock.toJson());
            //console.log('======================================================');

            blockStorageService.put(headBlock).catch(error => console.log(error));

            headBlock = new Block(
                headBlock.getBlockHash(),
                headBlock.blockHeader.timestamp,
                headBlock.blockHeader.difficulty_bits,
                headBlock.blockHeader.index + 1,
                CryptoUtils.getRandomString(100)
            );
        }
        headBlock.mine();
        minedBlocks.push(headBlock);
        blockStorageService.put(headBlock);

        /*blockStorageService.dbFacade.createReadStream()
            .on('data', function (data) {
                console.log(data.value, ',')
            })
            .on('end', function () {
                console.log('Stream ended')
            });*/
       
        return minedBlocks;
    }
}

export default MiningService;