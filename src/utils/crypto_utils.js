﻿import crypto from 'crypto'

class CryptoUtils {
    constructor() { }

    static getHash(data) {
        var hasher = crypto.createHash('SHA256');

        hasher.update(data);
        return hasher.digest('hex');
    }

    static getDoubleHash(data) {
        var firstHash = this.getHash(data);
        return this.getHash(firstHash);
    }

    static getRandomString(bytes) {
        return crypto.randomBytes(bytes).toString('hex');
    }
}

export default CryptoUtils;