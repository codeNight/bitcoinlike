﻿import CryptoUtils from '../utils/crypto_utils';
import BigInt from 'big-integer';

class BlockHeader {
    constructor(prev_hash, timestamp, difficulty_bits, index, payloadHash) {
        this.prev_hash = prev_hash;
        this.timestamp = timestamp;
        this.difficulty_bits = difficulty_bits;
        this.index = index;
        this.payloadHash = payloadHash;
    }
}

class Block {
    // Used with freshly created blocks
    constructor(prev_hash, timestamp, difficulty_bits, index, payload) {
        var payloadHash = CryptoUtils.getHash(payload);

        this.payload = payload;
        this.blockHeader = new BlockHeader(prev_hash, timestamp, difficulty_bits, index, payloadHash);
    }

    // assumes previously created block 
    static createFromJson(json) {
        // TODO: create blockHeader object, find how parse method work with nested objects.
    }

    toJson() {
        return JSON.stringify(this);
    }

    /**
     * Block hash only depends on pre-determined values of header
     */
    getBlockHash() {
        return CryptoUtils.getDoubleHash(JSON.stringify(this.blockHeader));
    }

    mine() {
        var target = BigInt(1).shiftLeft(256 - this.blockHeader.difficulty_bits);

        this.blockHeader.nonce = 0;

        var before = Date.now();

        do {
            this.blockHeader.nonce++;
            var hash = BigInt(this.getBlockHash(), 16);
        } while (hash.greaterOrEquals(target));

        this.timeElapsed = Date.now() - before;

        this.hash = this.getBlockHash();
        //console.log('final hash = ' + this.getBlockHash() + ', int_val = ' + BigInt(this.getBlockHash(), 16));
        //console.log('block mined successfully. target = ' + target + ', Block hash = ' + BigInt(this.getBlockHash(), 16) + ', iterations = ' + this.nonce + ', time required = ' + (this.timeElapsed));
        //console.log(this.toJson());
    }
}

export default Block;

