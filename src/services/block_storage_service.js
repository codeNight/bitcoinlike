﻿import Level from 'level';

class BlockStorageService {
    constructor(storeName = 'blocks') {
        this.dbFacade = Level(storeName, { createIfMissing: true }, function (err, db) {
            if (err) {
                console.log("Error failing for fuck sake: " + err);

                throw err;
            }
        });
    }

    async put(block) {
        var hash = block.getBlockHash();

        return this.dbFacade.put(hash, block.toJson()); 
    }

    async get(key) {
        var getPromise = this.dbFacade.get(key);

        return getPromise;
    }

    async close() {
        if (!this.dbFacade.isClosed()) {
            return this.dbFacade.close();
        }

        return Promise.resolve();
    }
}

export default BlockStorageService;
